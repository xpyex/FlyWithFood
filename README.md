# FlyWithFood

FlyWithFood是一款Minecraft插件，支持Bukkit与Sponge.它旨在让玩家拥有“飞行自由”！玩家可以通过消耗点数(饥饿值、经验等级、经验值、游戏币)来支撑飞行！详情请看:
https://www.mcbbs.net/thread-1253430-1-1.html (已死)

FlyWithFood is a Minecraft plugin that run with Bukkit or Sponge. <br>
It makes players can cost points(Saturation, EXP Levels, EXP Points, Money) to fly <br>
https://www.spigotmc.org/resources/flywithfood.99676/ <br>

<img src="https://github.com/0XPYEX0/FlyWithFood/blob/master/pictures/AutoDisable.png" />
<img src="https://github.com/0XPYEX0/FlyWithFood/blob/master/pictures/CanNotEnable.png" />
<img src="https://github.com/0XPYEX0/FlyWithFood/blob/master/pictures/Enabled.png" />
<img src="https://github.com/0XPYEX0/FlyWithFood/blob/master/pictures/CommandHelp.png" />

Thank you [@Amunak](https://github.com/Amunak) for your idea, to make FlyWithFood great again! <br>
The original project: https://github.com/Amunak/FlyWithFood <br>
原项目: https://github.com/Amunak/FlyWithFood 。当前FlyWithFood灵感来源于此
